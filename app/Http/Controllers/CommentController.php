<?php

namespace App\Http\Controllers;

use App\Comment;
use Auth;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function store(Request $request) {
        $validate = $request->validate([
            'comment' => 'required'
        ]);

        $post = Comment::create([
            'user_id' => Auth::user()->id,
            'post_id' => $request['post_id'],
            'comment' => $request['comment'],
        ]);

        // dd($post);
        // return view('post')->with('post', $post);
        return redirect()->back();
    }
}
