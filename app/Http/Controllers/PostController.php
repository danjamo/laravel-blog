<?php

namespace App\Http\Controllers;

use App\Post;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index() {
        $posts = Post::orderBy('updated_at', 'desc')->with(['user'])->get();
        // dd($posts);
        return view('welcome')->with('posts', $posts);
    }

    public function store(Request $request) {
        $validate = $request->validate([
            'title' => 'required',
            'content' => 'required'
        ]);

        $post = Post::create([
            'user_id' => Auth::user()->id,
            'blog_title' => $validate['title'],
            'blog_content' => $validate['content'],
        ]);

        return redirect('/home');
    }

    public function viewUserPosts() {
        $posts = Post::orderBy('updated_at', 'desc')->where('user_id', '=', Auth::user()->id)->get();
        return view('home')->with('posts', $posts);
    }

    public function show($id) {
        $post = Post::find($id)->with(['comments' => function($query) {
            return $query->orderBy('created_at', 'asc')->with(['user'])->get();
        }])->first();
        // $post = Post::find($id)->has('comments')->get();
        // $post->comments()->get();
        // dump($post);
        return view('post')->with('post', $post);
    }

    public function edit($id) {
        $post = Post::find($id);
        // dd($post);
        return view('editPost')->with('post', $post);
    }

    public function update(Request $request, $id)
    {
        $validate = $request->validate([
            'title' => 'required',
            'content' => 'required'
        ]);

        $post = Post::find($id);
        $post->blog_title = $validate['title'];
        $post->blog_content = $validate['content'];
        $post->save();

        return view('post')->with('post', $post);
    }
}