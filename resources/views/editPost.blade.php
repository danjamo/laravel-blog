@extends('layouts.app')

@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb small bg-transparent">
                <li class="breadcrumb-item"><a href="/">All Posts</a></li>
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Post {{ $post->id }}</li>
            </ol>
        </nav>
        <div class="d-flex flex-column my-4">
            <form action="{{ action('PostController@update', ['id' => $post->id]) }}" method="post">
                @csrf
                <div class="form-group">
                    <input type="text" name="title" id="title" value="{{ $post->blog_title }}" class="form-control @error('title') is-invalid @enderror">
                    @error('title')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <textarea name="content" id="content" rows="10" class="form-control @error('content') is-invalid @enderror">{{ $post->blog_content }}</textarea>
                </div>
                @error('content')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
@endsection