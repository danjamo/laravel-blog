@extends('layouts.app')

@section('content')
    <div class="container">
        @auth
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb small bg-transparent">
                    <li class="breadcrumb-item active" aria-current="page">All Posts</li>
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                </ol>
            </nav>
        @endauth
        <h3>All Blog Posts</h3>
        <hr>
        @if (count($posts) > 0)
            <div class="list-group">
                @foreach ($posts as $post)
                    <a href="/post/{{ $post->id }}" class="list-group-item list-group-item-action my-1 mh-25">
                        <div class="text-truncate mb-2 h5">{{ $post->blog_title }}</div>
                        <div class="text-truncate my-2">{{ $post->blog_content }}</div>
                        <div class="text-muted small d-flex flex-row">
                            <div>{{ $post->user->name }}</div>
                            <div class="ml-auto">
                                {{ date('d-m-Y', strtotime($post->user->updated_at)) }}
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        @else
            <h6 class="text-muted">No posts</h6>
        @endif
    </div>
@endsection