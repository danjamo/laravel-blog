@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb small bg-transparent">
            <li class="breadcrumb-item"><a href="/">All Posts</a></li>
            <li class="breadcrumb-item active" aria-current="page">Home</li>
        </ol>
    </nav>
    <h3 class="my-3">My Blog Post</h3>
    <hr>
    @if (count($posts) > 0)
        <div class="list-group">
            @foreach ($posts as $post)
                <a href="/post/{{ $post->id }}" class="list-group-item list-group-item-action my-1 mh-25 text-truncate">
                    <div class="mb-2 h5 text-truncate">
                        {{ $post->blog_title }}
                    </div>
                    <span class="text-muted">{{ $post->blog_content }}</span>
                </a>
            @endforeach
        </div>
    @else
        <h6 class="text-muted">You have no posts</h6>
    @endif
</div>
@endsection
