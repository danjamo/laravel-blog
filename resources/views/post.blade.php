@extends('layouts.app')

@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb small bg-transparent">
                <li class="breadcrumb-item"><a href="/">All Posts</a></li>
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Post {{ $post->id }}</li>
            </ol>
        </nav>
        <div class="d-flex flex-column my-4">
            <div class="d-flex flex-row alignt-items-center">
                <div class="h3 text-truncate pr-3">
                    {{ $post->blog_title }}
                </div>
                <div class="ml-auto">
                    @auth
                        @if (Auth::user()->id == $post->user_id)
                            <a href="{{ '/post/'.$post->id.'/edit' }}" class="btn btn-success btn-sm">Edit</a>
                        @endif
                    @endauth
                </div>
            </div>
            <p class="lead my-3">
                {{ $post->blog_content }}
            </p>
            <div class="text-muted d-flex flex-column">
                <span>{{ $post->user->name }}</span>
                <span>{{ $post->user->updated_at }}</span>
            </div>
        </div>
        @guest
            <div class="alert alert-secondary" role="alert">
                Please <a href="/login">login</a> to comment or <a href="/register">create</a> a new account
            </div>
        @else
            <div class="card">
                <div class="card-body">
                    <div class="my-2">
                        <form action="{{ url('/post/'.$post->id.'') }}" method="post">
                            @csrf
                            <input type="hidden" name="post_id" value="{{ $post->id }}">
                            <div class="form-group">
                                <textarea name="comment" id="comment" rows="2" placeholder="Your comment" class="form-control @error('comment') is-invalid @enderror">{{ old('comment') }}</textarea>
                                @error('comment')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-primary btn-sm">Post comment</button>
                        </form>
                    </div>
                    <hr>
                    @if (count($post->comments) > 0)
                        @foreach ($post->comments as $comment)
                            <div class="d-flex flex-column bg-light border p-3 my-2">
                                <div class="d-flex flex-row overflow-hidden">
                                    <div class="text-truncate pr-4">
                                        <b>{{ $comment->user->name }}</b>
                                    </div>
                                    <div class="ml-auto text-muted small">
                                        {{ date('m-d-Y', strtotime($comment->created_at)) }}
                                    </div>
                                </div>
                                <div class="">
                                    {{ $comment->comment }}
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        @endguest
    </div>
@endsection